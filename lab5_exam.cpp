#include <iostream>
#include <math.h>
#include <map>

// відсортувати головну діагональ матриці методом обміну
int main()
{
    //   main 1,4,8,7,1
    // expect 1,1,4,7,8
    int a[5][5] = {
        {1, 2, 3, 4, 5},
        {5, 4, 3, 2, 1},
        {6, 7, 8, 9, 0},
        {0, 9, 8, 7, 6},
        {1, 1, 1, 1, 1}};

    for (int i = 0; i < 5; i++)
    {
        for (int j = i + 1; j < 5; j++)
        {
            if (a[i][i] > a[j][j])
            {
                int tmp = a[i][i];
                a[i][i] = a[j][j];
                a[j][j] = tmp;
            }
        }
    }

    printf("\n");
    for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            printf("%i ", a[i][j]);
        }
        printf("\n");
    }
    return 0;
}
