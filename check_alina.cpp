/*
	Знайти вузол, сума цифр ключа якого є мінімальною серед кратних 5.
	125, 722, 769, 349, 964, 417, 77, 5, 673, 471, 811, 31, 697, 359, 807, 297, 297, 663, 990, 2
*/

#include <stdio.h>
#include <stdlib.h>


typedef struct node {
	int data;
	struct node *left;
	struct node *right;
} Node;


typedef Node *Bin_tree;


int  *init_array(int, FILE*);
int   count_numbers_in_file(FILE*);
Node *new_node(int);
Node *insert(Node*, int);
void  printPreorder(Node*);
void  printInorder(Node*);
void  printPostorder(Node*);
int   tree_height(Node* );
void  print_level_order(Node*);
void  print_given_level(Node*, int);
void  parse_number_to_digits(int, int*);
int   get_digits_sum_from_number(int);
int   get_first_number_mod5(int*, int, int*);
void  get_min_sum_of_keys(Node*, int*);
void  free_node_memory(Node*);
void  free_tree_memory(Node*);
void  free_memory(int**);
void  free_arr(int*, int);


int main()
{
	Bin_tree tree = NULL;

	int i,
		tree_data_size = 0,
		min = 1515151;
	int *tree_data;

	// getting data from file and put it into array tree_data
	FILE *file = fopen("bin_tree.txt", "r");
	if (file == NULL)
	{
		printf("Error. File doesn't found\n");
		exit(0);
	}
	tree_data_size = count_numbers_in_file(file);
	tree_data = init_array(tree_data_size, file);
	fclose(file);
	//

	// making tree from array
	tree = new_node(tree_data[0]);
	for (i = 1; i < tree_data_size; i++)
	{
		insert(tree, tree_data[i]);
	}
	//

	printf("\n\n*** Preorder: ***\n");
	printPreorder(tree);

	printf("\n\n*** Inorder: ***\n"); 
	printInorder(tree);

	printf("\n\n*** Postorder: ***\n");
	printPostorder(tree);

	printf("\n\n*** Levelorder: ***\n");
	print_level_order(tree);

	printf("\n\n*** Multitples of five : ***\n");
	// get firts number fot searching
	min = get_first_number_mod5(tree_data, tree_data_size, &min);
	// searching
	get_min_sum_of_keys(tree, &min);
	printf("\nAnd minimum is: %d\n", min);

	free_memory(&tree_data);
	free_tree_memory(tree);
	free_arr(tree_data, tree_data_size);
	return 0;
}


int *init_array(int size, FILE* file)
{
	int num;
	int i = 0;
	int *array = (int *)malloc(size * sizeof(int));

	rewind(file);
	fseek(file, 0, SEEK_SET);

	printf("\nInput data:\n");

	while (fscanf(file, "%d", &num) > 0)
	{
		array[i] = num;
		printf("%d  ", num);
		i++;
	}
	printf("\n");
	return array;
}


int count_numbers_in_file(FILE *file)
{
	int num;
	int count = 0;
	while(fscanf(file, "%d", &num) > 0)
		count++;
	return count;
}


Node *new_node(int data)
{
	Node *node;
	node = (Node*) malloc(sizeof( Node));
	node->data = data;
	node->left = NULL;
	node->right = NULL;

	return node;
}


Node* insert(Node *root, int data)
{
	if (root == NULL)
		return new_node(data);
	else if (data > root->data)
		root->right = insert(root->right, data);
	else
		root->left = insert(root->left, data);
	return root;
}


void printPreorder(Node *root)
{
	if (root != NULL)
	{
		printf("%d  ", root->data);
		printPreorder(root->left);
		printPreorder(root->right);
	}
}


void printInorder(Node *root)
{
	if (root != NULL)
	{
		printInorder(root->left);
		printf("%d  ", root->data);
		printInorder(root->right);
	}
}


void printPostorder(Node *root)
{
	if (root != NULL)
	{
		printPostorder(root->left);
		printPostorder(root->right);
		printf("%d  ", root->data);
	}
}


int tree_height(Node* root)
{
	if (root == NULL)
	{
		return 0;
	} else {
		int leftH = tree_height(root->left);
		int rightH = tree_height(root->right);
 
		if (leftH > rightH)
			return ( leftH + 1 );
		else return ( rightH + 1 );
	}
}


void print_level_order(Node* root)
{
	int height = tree_height(root);
	int i;
	for (i = 1; i <= height; i++)
		print_given_level(root, i);
}


void print_given_level(Node* root, int level)
{
	if (root == NULL)
		return;
	if (level == 1)
	{
		printf("%d  ", root->data);
	} else if (level > 1) {
		print_given_level(root->left, level-1);
		print_given_level(root->right, level-1);
	}
}


int get_digits_sum_from_number(int digit) {
	int numb = 0,
		tmp = digit;

	while (tmp != 0)
	{
		numb += tmp%10;
		tmp /= 10;
	}

	return numb;
}


int get_first_number_mod5(int *tree_data, int tree_data_size, int *min)
{
	int i = 0;

	while (i < tree_data_size)
	{
		if (get_digits_sum_from_number(tree_data[i]) % 5 == 0)
			return tree_data[i];
		i++;
	}

	return -1;
}


void parse_number_to_digits(int digit, int *min)
{
	int numb = get_digits_sum_from_number(digit);

	if (numb % 5 == 0)
	{
		printf("%d  ", digit);
		if (*min > digit)
			*min = digit;
	}
}


void get_min_sum_of_keys(Node *root, int *min)
{
	if (root != NULL)
	{
		parse_number_to_digits(root->data, min);
		get_min_sum_of_keys(root->left, min);
		get_min_sum_of_keys(root->right, min);
	}
}


void free_node_memory(Node *root)
{
	free(root);
}


void free_tree_memory(Node *root)
{
	if (root != NULL)
	{
		free_tree_memory(root->left);
		free_tree_memory(root->right);
		free_node_memory(root);
	}
}


void free_memory(int **pointer)
{
	free(*pointer);
	*pointer = NULL;
}


void free_arr(int *arr, int size)
{
	int i = 0;
	while (i < size)
	{
		free(arr);
		i++;
	}
}