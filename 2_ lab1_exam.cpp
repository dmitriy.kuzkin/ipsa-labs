#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    int n = 10000;
    int **a = new int *[n];
    // srand((unsigned int)time(NULL)); // for random each call program run
    for (int i = 0; i < n; i++)
    {
        a[i] = new int[n];
        for (int j = 0; j < n; j++)
        {
            a[i][j] = rand() % 10;
        }
    }
    long res = 0;
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            res += a[i][j];
        }
    }
    cout << "Сумма элементів вище головноЇ діагоналі : " << res << endl;
    return 0;
}
