#include <stdio.h>
#include <iostream>
#include <tuple>
#include <time.h>

using namespace std;

// "Дано: натуральне число n, символи S1,...,Sn. Визначити кількість вхождень у послідовність S1,...,Sn груп літер " abc ""
int main()
{
    int n = 512;
    string input;
    for (int i = 0; i < n; i++)
    {
        // 0 - 31(Control characters), 127-delete char
        // 0-127 so go always 32-126 (idx % (126-32) + 32)
        input += (char)(i % 94) + 32;
    }

    string search = "abc";

    int count;
    for (int i = 0; i < input.length(); i++)
    {
        for (int j = 0, i2 = i; j < search.length();)
        {
            if (input[i2] == search[j])
            {
                if (j + 1 == search.length()) // all word found
                {
                    i = i2;
                    count += 1;
                    break;
                }
                else // not all word found
                {
                    j += 1;
                    i2 += 1;
                }
            }
            else // char at phrase not as char at world, next search
            {
                i = i2;
                break;
            }
        }
    }
    cout << "у фразі : " << input << endl;
    cout << "abc знайдено разів : " << count << endl;

    return 0;
}
