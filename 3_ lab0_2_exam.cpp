#include <stdio.h>
#include <iostream>

using namespace std;

// "Надано два впорядкованих масиви 1 2 1 2 , n m a a a b b b       
// Створити новий впорядкований масив 1 2 n m c c c    . Сортування жодного масиву не виконувати."
int main()
{
    int a[5] = {1, 2, 3, 4, 5};
    int b[10] = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20};
    int c[15];
    int idxA = 0, idxB = 0;
    for (int i = 0; i < 15; i++)
    {
        if (idxA < 5 && a[idxA] <= b[idxB])
        {
            c[i] = a[idxA];
            idxA++;
        }
        else
        {
            c[i] = b[idxB];
            idxB++;
        }
    }

    cout << "Массив c" << endl;
    for (int i = 0; i < 15; i++)
    {
        cout << c[i] << " ";
    }
    return 0;
}