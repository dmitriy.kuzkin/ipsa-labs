#include <stdio.h>
#include <iostream>

using namespace std;

// Надано 2 впорядкованих массиви
// a1 <= a2 <= K <= an
// b1 <= b2 <= K <= bm
// Створити новий впорядкованих массив(сортування жодного массиву не виконувати)
// C1 <= C2 <= K <= c(m+n)
int main()
{
    int a[5] = {1, 2, 3, 4, 5};
    int b[10] = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20};
    int c[15];
    int idxA = 0, idxB = 0;
    for (int i = 0; i < 15; i++)
    {
        if (idxA < 5 && a[idxA] <= b[idxB])
        {
            c[i] = a[idxA];
            idxA++;
        }
        else
        {
            c[i] = b[idxB];
            idxB++;
        }
    }

    cout << "Массив c" << endl;
    for (int i = 0; i < 15; i++)
    {
        cout << c[i] << " ";
    }
    return 0;
}
