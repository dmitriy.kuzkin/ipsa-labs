#include <iostream>
#include <string>
// #include <algorithm>

// Реалізувати функцію, у яку передається деякий масив цілих чисел та його
// розмір і яка підраховує кількість елементів цього масиву, які задовольняють
// певній умові. Цю умову передавати як вказівник на функцію. Передбачити дві
// функції – визначення чи є дане число простим, і визначення чи є дане число
// парним. Продумати тестовий приклад для демонстрації роботи
using namespace std;

bool isPrime(int n)
{ // algorith complexity is not a part of this lab
    if (n <= 1)
        return false;
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return false;
    return true;
}

bool isEven(int n)
{
    return n % 2 == 0;
}

int check(int arr[], int length, bool (*predicate)(int))
{
    int total = 0;
    for (int n = 0; n < length; n++)
    {
        if (predicate(arr[n]))
            total += 1;
    }
    return total;
}

int main()
{
    int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

    cout << check(a, 20, isEven) << endl;  // 10(2,4,6....20)
    cout << check(a, 20, isPrime) << endl; //  8(2,3,5,7,11,13,17,19)
    return 0;
}
