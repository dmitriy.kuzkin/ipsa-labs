#include <stdio.h>
#include <vector>
#include <sstream>
#include <iterator>
#include <iostream>

using namespace std;

// Розробити алгоритм і програму швидкого лінійного пошуку.
// В якості вхідних даних слід використовувати рядок тексту,
// з якого необхідно виділити слова. Потім слова впорядкувати за алфавітом.
// Аргумент пошуку – слово.

string inputPhrase()
{
    cout << "Введіть фразу" << endl;
    string phrase;
    getline(cin, phrase);
    return phrase;
}

string inputWord()
{
    cout << "Введіть слово" << endl;
    string word;
    getline(cin, word);
    return word;
}

vector<string> splitBySpace(string phrase)
{
    string str = "";
    vector<string> words;
    for (int i = 0; i < phrase.length(); i++)
    {
        if (phrase[i] != ' ')
        {
            str += phrase[i];
            if (i == phrase.length() - 1) // останній символ
                words.push_back(str);
        }
        else if (str.length() > 0)
        {
            words.push_back(str);
            str = "";
        }
    }
    return words;
}

void insertionSort(vector<string> &arr)
{
    int i, j, n = arr.size();
    string el;
    for (i = 1; i < n; i++)
    {
        el = arr[i];
        j = i - 1;
        while (j >= 0 && arr[j] > el)
        {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = el;
    }
}
// насправді для лінійного пошуку
// ми можемо відразу проходитись по рядку і порівнювати слова
// робимо по завданню задля закріплення навичок
// тому присутне сортування
int main()
{

    string phrase = inputPhrase();
    string word = inputWord();
    vector<string> phraseWords = splitBySpace(phrase); // 1 розбираемо рядок і виділяемо слова
    insertionSort(phraseWords);                        // 2 робимо сортування (сортування вставкою)
    // 3 лінійний пошук
    string res = "слово відсутнє";
    for (int i = 0; i < phraseWords.size(); i++)
    {
        if (phraseWords[i] == word)
        {
            res = "слово присутнє";
            break;
        }
    }

    std::ostringstream vts;
    copy(phraseWords.begin(), phraseWords.end(), std::ostream_iterator<string>(vts, ", "));
    string sortedPhraseString = vts.str();

    cout << "Пошук слова: " << word << endl;
    cout << "По фразі: " << phrase << endl;
    cout << "Відсортовані слова фрази: " << sortedPhraseString << endl;
    cout << res << endl;

    return 0;
}
