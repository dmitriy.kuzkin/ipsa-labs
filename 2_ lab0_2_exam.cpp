#include <stdio.h>
#include <iostream>
#include <tuple>
#include <time.h>

using namespace std;

int *mkArray(int size, string fillType)
{
    int *a = new int[size];
    if (fillType == "random")
    {
        srand((unsigned int)time(NULL)); // for random each call program run
        for (int i = 0; i < size; i++)
            a[i] = rand();
    }
    else if (fillType == "sorted_up")
    {
        int tmp = 0;
        for (int i = 0; i < size; i++)
            a[i] = tmp++;
    }
    else if (fillType == "sorted_down")
    {
        int tmp = size;
        for (int i = 0; i < size; i++)
            a[i] = tmp--;
    }
    else
        throw invalid_argument("not know how to fill array");

    return a;
}

void heapify(int *arr, int n, int i, tuple<long, long> &checksSwaps)
{
    int largest = i;   // Initialize largest as root
    int l = 2 * i + 1; // left = 2*i + 1
    int r = 2 * i + 2; // right = 2*i + 2

    // If left child is larger than root
    if (l < n && arr[l] > arr[largest])
    {
        largest = l;
        get<0>(checksSwaps) += 1;
    }

    // If right child is larger than largest so far
    if (r < n && arr[r] > arr[largest])
    {
        largest = r;
        get<0>(checksSwaps) += 1;
    }

    // If largest is not root
    if (largest != i)
    {
        swap(arr[i], arr[largest]);
        get<0>(checksSwaps) += 1;
        get<1>(checksSwaps) += 1;
        // Recursively heapify the affected sub-tree
        heapify(arr, n, largest, checksSwaps);
    }
}

tuple<long, long> heapSort(int arr[], int n)
{
    tuple<long, long> checksSwaps;
    // Build heap (rearrange array)
    for (int i = n / 2 - 1; i >= 0; i--)
    {
        heapify(arr, n, i, checksSwaps);
        get<0>(checksSwaps) += 1;
        get<1>(checksSwaps) += 1;
    }

    // One by one extract an element from heap
    for (int i = n - 1; i > 0; i--)
    {
        // Move current root to end
        swap(arr[0], arr[i]);
        get<1>(checksSwaps) += 1;

        // call max heapify on the reduced heap
        heapify(arr, i, 0, checksSwaps);
    }
    return checksSwaps;
}

void runHeapSort(int n, string fillType)
{
    int *a = mkArray(n, fillType);

    tuple<long, long> tup = heapSort(a, n);
    cout << "  Сортування пірамідальне " << n << " елементів" << endl;
    cout << "              порівнянь : " << get<0>(tup) << endl;
    cout << "                обмінів : " << get<1>(tup) << endl;
    
    // bool ok = true;
    // for (int i = 1; i < n; i++)
    //     if (a[i - 1] > a[i])
    //         ok = false;
    // cout << "sorted" << endl;
    // cout << ok << endl;
    // cout << "___" << endl;

    delete[] a;
}

int main()
{
    // TODO check what is best what is worst
    cout << "--- Сортування пірамідальне ---" << endl;
    cout << "Найкращий випадок" << endl;
    runHeapSort(1000, "sorted_up");
    runHeapSort(10000, "sorted_up");
    runHeapSort(100000, "sorted_up");

    cout << "Найгірший випадок" << endl;
    runHeapSort(1000, "sorted_down");
    runHeapSort(10000, "sorted_down");
    runHeapSort(100000, "sorted_down");

    cout << "Середній випадок" << endl;
    runHeapSort(1000, "random");
    runHeapSort(10000, "random");
    runHeapSort(100000, "random");

    return 0;
}
