// Для однозв’язного списку точок в R^3 реалізувати функції:
// а) додавання елемента у порядку зростання суми квадратів координат;
// б) видалення елемента з голови списку;
// в) змінити порядок слідування елементів;
// г) надрукувати весь список, формат: “Point(x, y, z)”;
// д) видалити весь список.

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>

using namespace std;

class Point
{
public:
    int x, y, z;

public:
    Point(int x, int y, int z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }
    int sumSquareCoords()
    {
        return (this->x * this->x) + (this->y * this->y) + (this->z * this->z);
    }
    bool isGreaterBySumSquareCoords(Point *other)
    {
        return this->sumSquareCoords() > other->sumSquareCoords();
    }
    string toString()
    {
        return "Point(" + to_string(x) + "," + to_string(y) + "," + to_string(z) + ")";
    }
};

class Node
{
public:
    Point *head;
    Node *tail;
    Node(Point *x)
    {
        head = x;
    }
    Node(Point *x, Node *xs)
    {
        head = x;
        tail = xs;
    }
};
class PointList
{
public:
    Node *node;

public:
    PointList(Point *key)
    {
        this->node = new Node(key);
    }
    void addOrededBySumSquareCoords(Point *point)
    {
        Node *n = this->node;
        do
        {
            if (n == NULL)
            {
                n = new Node(point);
                break;
            }
            if (n->head->isGreaterBySumSquareCoords(point))
            {
                n->tail = new Node(n->head, n->tail);
                n->head = point;
                break;
            }
            if (n->tail == NULL)
            {
                n->tail = new Node(point);
                break;
            }
            n = n->tail;
        } while (true);
    }
    void rmHead()
    {
        if (node != NULL)
        {
            Node *res = node->tail;
            delete node;
            node = res;
        }
    }
    void clean()
    {
        do
        {
            rmHead();
        } while (node != NULL);
    }
    void reverse()
    {
        cout << "before reverse " << this->toString() << endl;
        Node *p = NULL;
        Node *c = node;
        Node *n = NULL;
        while (c != NULL)
        {
            n = c->tail;
            c->tail = p;
            p = c;
            c = n;
        }
        node = p;
    }
    string toString()
    {
        string res = "";
        Node *n = node;
        do
        {
            if (n != NULL && n->head != NULL)
            {
                res += n->head->toString();
                if (n->tail != NULL)
                    res += ",";
                n = n->tail;
            }
            else
                break;
        } while (true);
        return res;
    }
};

int main()
{
    cout << "\nЛіст с одним елементом (1.1.1)\n";
    PointList *list = new PointList(new Point(1, 1, 1));
    cout << list->toString() << endl;

    cout << "\nДодаємо (2.2.2)\n";
    list->addOrededBySumSquareCoords(new Point(2, 2, 2));
    cout << list->toString() << endl;

    cout << "\nДодаємо (0.0.0)\n";
    list->addOrededBySumSquareCoords(new Point(0, 0, 0));
    cout << list->toString() << endl;

    cout << "\nРозгортаємо\n";
    list->reverse();
    cout << list->toString() << endl;

    cout << "\nВидаляємо перший\n";
    list->rmHead();
    cout << list->toString() << endl;

    cout << "\nВидаляємо весь лист\n";
    list->clean();
    cout << list->toString() << endl;

    return 0;
}