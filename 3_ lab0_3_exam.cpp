#include <iostream>
#include <math.h>
#include <map>

using namespace std;

// "Дано двовимірний цілочисельний масив А[N][М]. Відомо, що серед його елементів тільки два рівних між собою. Знайти їх та вивести на друк їх індекси." return 0;
int main()
{
    int l1 = 4;
    int l2 = 2;
    int a[l1][l2] = {
        {1, 2},
        {3, 4},
        {5, 4},
        {6, 7},
    };

    using namespace std;
    map<int, tuple<int, int>> coordByValue;
    map<int, tuple<int, int>>::iterator it;
    for (int i = 0; i < l1; i++)
    {
        for (int j = 0; j < l2; j++)
        {
            it = coordByValue.find(a[i][j]);
            if (it != coordByValue.end())
            {
                printf("\n [%i][%i] , [%i][%i] \n", get<0>(it->second), get<1>(it->second), i, j);
                return 0;
            }
            else
            {
                coordByValue[a[i][j]] = {i, j};
            }
        }
    }
    printf("not found\n");
    return 1;
}
