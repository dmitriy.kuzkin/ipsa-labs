#include <iostream>
#include <math.h>
#include <stdio.h>
using namespace std;

int main()
{
    cout << "R = sin(x) * e^(2y)" << endl;

    double x, y;
    cout << "Введіть значення x" << endl;
    scanf("%lf", &x);

    cout << "Введіть значення y" << endl;
    scanf("%lf", &y);

    double r = sin(x) * pow(M_E, 2 * y);
    cout << "R =" << r << endl;
    return 0;
}
