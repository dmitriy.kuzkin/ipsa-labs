#include <stdio.h>
#include <iostream>
#include <tuple>
#include <time.h>

using namespace std;

int *mkArray(int size, string fillType)
{
    int *a = new int[size];
    if (fillType == "random")
    {
        srand((unsigned int)time(NULL)); // for random each call program run
        for (int i = 0; i < size; i++)
            a[i] = rand();
    }
    else if (fillType == "sorted_up")
    {
        int tmp = 0;
        for (int i = 0; i < size; i++)
            a[i] = tmp++;
    }
    else if (fillType == "sorted_down")
    {
        int tmp = size;
        for (int i = 0; i < size; i++)
            a[i] = tmp--;
    }
    else
        throw invalid_argument("not know how to fill array");

    return a;
}

tuple<long, long> selectionSort(int arr[], int n)
{
    int i, j, minIdx;
    unsigned long checks = 0, swaps = 0;

    for (i = 0; i < n - 1; i++)
    {
        minIdx = i;
        for (j = i + 1; j < n; j++)
        {
            if (arr[j] < arr[minIdx])
            {
                minIdx = j;
            }
            checks++;
        }
        swap(arr[minIdx], arr[i]);
        swaps++;
    }
    return make_tuple(checks, swaps);
}

void runSelectionSort(int n, string fillType)
{
    int *a = mkArray(n, fillType);

    tuple<long, long> tup = selectionSort(a, n);
    cout << "  Сортування вибіром " << n << " елементів" << endl;
    cout << "         порівнянь : " << get<0>(tup) << endl;
    cout << "           обмінів : " << get<1>(tup) << endl;
    bool ok = true;
    for (int i = 1; i < n; i++)
        if (a[i - 1] > a[i])
            ok = false;
    cout << "sorted" << endl;
    cout << ok << endl;
    cout << "___" << endl;


    delete[] a;
}

int main()
{
    cout << "--- Сортування вставкою ---" << endl;
    cout << "Найкращий випадок" << endl;
    runSelectionSort(1000, "sorted_up");
    runSelectionSort(10000, "sorted_up");
    runSelectionSort(100000, "sorted_up");

    cout << "Найгірший випадок" << endl;
    runSelectionSort(1000, "sorted_down");
    runSelectionSort(10000, "sorted_down");
    runSelectionSort(100000, "sorted_down");

    cout << "Середній випадок" << endl;
    runSelectionSort(1000, "random");
    runSelectionSort(10000, "random");
    runSelectionSort(100000, "random");

    return 0;
}
