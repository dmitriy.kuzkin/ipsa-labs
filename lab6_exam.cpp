#include <iostream>
#include <string>
#include <algorithm>
// З клавіатури увести фразу.
// Викинути із усіх речень фрази деяке слово(також уводиться з клавіатури).
// Різницю в регістрах ігнорувати
using namespace std;

int main()
{
    cout << "Введіть фразу" << endl;
    string p;
    getline(cin, p);

    cout << "Введіть слово" << endl;
    string w;
    getline(cin, w);
    // string p = "hello World wo rld blaworld worWorL";
    // string w = "WoRLD";
    // expect "hello wo rld bla worL"
    string res = "";
    for (int i = 0; i < p.length(); i++)
    {
        for (int j = 0, i2 = i; j < w.length();)
        {
            if (tolower(p[i2]) == tolower(w[j]))
            {
                if (j + 1 == w.length()) // all word found
                {
                    i = i2;
                    break;
                }
                else // not all word found
                {
                    j += 1;
                    i2 += 1;
                }
            }
            else // char at phrase not as char at world, add phrase processing chars
            {
                res += p.substr(i, j + 1);
                i = i2;
                break;
            }
        }
    }
    cout << "фраза    :" << p << endl;
    cout << "слово    :" << w << endl;
    cout << "результат:" << res << endl;
    return 0;
}
