#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Node
{
public:
	int value;
	Node *left;
	Node *right;
};

vector<int> inputData()
{
	ifstream file("3_lab3_bin_tree.txt");
	vector<int> res;
	if (!file.is_open())
	{
		cout << "Failed to open input data file";
		exit(1);
	}
	int value;
	while (file >> value)
		res.push_back(value);
	file.close();
	if (res.size() == 0)
	{
		cout << "Input empty";
		exit(1);
	}
	return res;
}

Node *newNode(int data)
{
	Node *res = new Node();
	res->value = data;
	return res;
}

Node *insert(Node *root, int value)
{
	if (root == NULL)
		return newNode(value);
	else if (root->value < value)
		root->right = insert(root->right, value);
	else
		root->left = insert(root->left, value);
	return root;
}

void printPreorder(Node *node)
{
	if (node != NULL)
	{
		cout << node->value << " ";
		printPreorder(node->left);
		printPreorder(node->right);
	}
}

void printInorder(Node *node)
{
	if (node != NULL)
	{
		printInorder(node->left);
		cout << node->value << " ";
		printInorder(node->right);
	}
}

void printPostorder(Node *node)
{
	if (node != NULL)
	{
		printPostorder(node->left);
		printPostorder(node->right);
		cout << node->value << " ";
	}
}

int nodeHeight(Node *node)
{
	if (node == NULL)
		return 0;
	else
	{
		int lheight = nodeHeight(node->left);
		int rheight = nodeHeight(node->right);
		if (lheight > rheight)
			return (lheight + 1);
		else
			return (rheight + 1);
	}
}

void printGivenLevel(Node *root, int level)
{
	if (root == NULL)
		return;
	if (level == 1)
		cout << root->value << " ";
	else if (level > 1)
	{
		printGivenLevel(root->left, level - 1);
		printGivenLevel(root->right, level - 1);
	}
}

void printLevelOrder(Node *node)
{
	int height = nodeHeight(node);
	for (int i = 1; i <= height; i++)
	{
		printGivenLevel(node, i);
		cout << endl;
	}
}

void fillLevelValues(Node *node, int level, vector<int> &res)
{
	if (node == NULL)
		return;
	if (level == 1)
		res.push_back(node->value);
	else if (level > 1)
	{
		fillLevelValues(node->left, level - 1, res);
		fillLevelValues(node->right, level - 1, res);
	}
}

int main()
{
	vector<int> input = inputData();
	Node *root = newNode(input[0]);
	cout << "\n Будуємо дерево із " << input.size() << endl;
	for (vector<int>::size_type i = 1; i != input.size(); i++)
		insert(root, input[i]);

	cout << "\n Прямий(root->left->right) \n";
	printPreorder(root);

	cout << "\n Симетричний(відсортований порядок(left->root->right)) \n";
	printInorder(root);

	cout << "\n Зворотній(left->right->root) \n";
	printPostorder(root);

	cout << "\n По рінях(як відображені\n";
	printLevelOrder(root);

	cout << "\nОбчислити добуток ключів вузлів третього рівня дерева.\n";
	vector<int> accum;
	fillLevelValues(root, 3, accum);
	int res = 0;
	if (accum.size() > 0)
	{
		res = 1;
		for (vector<int>::size_type i = 0; i != accum.size(); i++)
			res *= accum[i];
	}
	cout << "\nДобуток дорівнює:" << res << endl;

	return 0;
}
