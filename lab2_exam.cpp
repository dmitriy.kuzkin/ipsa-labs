#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    cout << "\nРозв`язок рівняння" << endl;
    cout << "1/a^2 якщо a <= -1" << endl;
    cout << "a^2   якщо -1 < a <= 2" << endl;
    cout << "4     якщо a > 2" << endl;

    cout << "Введіть значення 'a'" << endl;
    double a;
    scanf("%lf", &a);

    if (a <= -1)
        cout << 1 / pow(a, 2) << endl;
    else if (a <= 2)
        cout << pow(a, 2) << endl;
    else
        cout << "4" << endl;
    return 0;
}
