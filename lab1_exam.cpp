#include <math.h>
#include <stdio.h>

int main()
{
    printf("| (sin(|c*x1^3 + dx2^2 - cd|)^3) / ( (cx1^3 + dx2^2 - x1)^2 + 3.14)^0.5 | \n");
    printf("Введіть значення 'c' та 'd' через пропуск\n");
    int c, d;
    scanf("%d%d", &c, &d);

    printf("X1 та X2 знаходяться із рівняння x^2 - 3x -%d\n", c * d);
    long discriminant = pow(-3, 2) - (4 * 1 * c * d);
    if (discriminant < 0)
    {
        printf("Квадратичне рівняння не мае розв'язків, діскрімінант %ld\n", discriminant);
        return 1;
    }
    else
    {
        double x1 = (-3 + sqrt(discriminant)) / 2;
        double x2 = (-3 - sqrt(discriminant)) / 2;
        printf("x1=%lf x2=%lf\n", x1, x2);
        double res = abs(
            pow(sin(abs((c * pow(x1, 3)) + (d * pow(x2, 2)) - (c * d))), 3) /
            sqrt(pow(((c * pow(x1, 3)) + (d * pow(x2, 2)) - x1), 2) + 3.14));
        printf("Розв'язок : %lf\n", res);
        return 0;
    }
}
