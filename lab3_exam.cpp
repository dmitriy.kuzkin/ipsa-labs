#include <iostream>
#include <math.h>

using namespace std;
int main()
{
    double eps = 0.000001;
    double x = 30;

    double chunk = x;
    double res = 0.0;
    int n = 1;
    while (abs(chunk) > abs(eps))
    {
        res += chunk;
        chunk *= (x / (n + 1)) * (x / (n + 2));
        n += 2;
    }

    cout << "результат sinh(x)    " << sinh(x) << endl;
    cout << "результат розрахунку " << res << endl;
    cout << "різниця              " << sinh(x) - res << endl;

    return 0;
}
