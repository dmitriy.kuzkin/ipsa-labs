#include <stdio.h>
#include <iostream>
#include <tuple>
#include <time.h>

using namespace std;

int *mkArray(int size, string fillType)
{
    int *a = new int[size];
    if (fillType == "random")
    {
        srand((unsigned int)time(NULL)); // for random each call program run
        for (int i = 0; i < size; i++)
            a[i] = rand();
    }
    else if (fillType == "sorted_up")
    {
        int tmp = 0;
        for (int i = 0; i < size; i++)
            a[i] = tmp++;
    }
    else if (fillType == "sorted_down")
    {
        int tmp = size;
        for (int i = 0; i < size; i++)
            a[i] = tmp--;
    }
    else
        throw invalid_argument("not know how to fill array");

    return a;
}

// A function to do counting sort of arr[] according to
// the digit represented by exp.
void countSort(int *arr, int n, int exp, tuple<long, long> &checksSwaps)
{
    int output[n]; // output array
    int i, count[10] = {0};

    // Store count of occurrences in count[]
    for (i = 0; i < n; i++)
    {
        count[(arr[i] / exp) % 10]++;
        get<0>(checksSwaps) += 1;
    }

    // Change count[i] so that count[i] now contains actual
    //  position of this digit in output[]
    for (i = 1; i < 10; i++)
    {
        count[i] += count[i - 1];
        get<0>(checksSwaps) += 1;
    }

    // Build the output array
    for (i = n - 1; i >= 0; i--)
    {
        output[count[(arr[i] / exp) % 10] - 1] = arr[i];
        count[(arr[i] / exp) % 10]--;
        get<0>(checksSwaps) += 1;
    }

    // Copy the output array to arr[], so that arr[] now
    // contains sorted numbers according to current digit
    for (i = 0; i < n; i++)
    {
        arr[i] = output[i];
        get<0>(checksSwaps) += 1;
        get<1>(checksSwaps) += 1;
    }
}

tuple<long, long> radixSort(int *arr, int n)
{
    int j, el;
    tuple<long, long> checksSwaps;
    // Find the maximum number to know number of digits
    int m = arr[0];
    for (int i = 1; i < n; i++)
        if (arr[i] > m)
            m = arr[i];

    // Do counting sort for every digit. Note that instead
    // of passing digit number, exp is passed. exp is 10^i
    // where i is current digit number
    for (int exp = 1; m / exp > 0; exp *= 10)
        countSort(arr, n, exp, checksSwaps);

    return checksSwaps;
}

void runRadixSort(int n, string fillType)
{
    int *a = mkArray(n, fillType);

    tuple<long, long> tup = radixSort(a, n);
    cout << "  Сортування підрахунком " << n << " елементів" << endl;
    cout << "               порівнянь : " << get<0>(tup) << endl;
    cout << "                 обмінів : " << get<1>(tup) << endl;

    delete[] a;
}

int main()
{
    cout << "--- Сортування підрахунком ---" << endl;
    cout << "Найкращий випадок" << endl;
    runRadixSort(1000, "sorted_up");
    runRadixSort(10000, "sorted_up");
    runRadixSort(100000, "sorted_up");

    cout << "Найгірший випадок" << endl;
    runRadixSort(1000, "sorted_down");
    runRadixSort(10000, "sorted_down");
    runRadixSort(100000, "sorted_down");

    cout << "Середній випадок" << endl;
    runRadixSort(1000, "random");
    runRadixSort(10000, "random");
    runRadixSort(100000, "random");

    return 0;
}
